/*
 * initDef.h
 *
 *  Created on: 11.05.2018
 *      Author: reinhard
 */

#ifndef STORE_INITDEF_H_
#define STORE_INITDEF_H_

#include <inttypes.h>
#include <String.h>
#include <map>
// #include <set>

namespace init_def
{
		static const std::map<String,String> _initDefault =
		{
			/*
			 * SYSTEM CONF
			 */
			/*
			 * NET CONF
			 */
			
			{  "GW_ACC" , "192.168.5.1" },
			{  "IP_ACC" , "192.168.5.1" },
			{  "NM_ACC" , "255.255.255.0" },
			{  "SSID_ACC", "MYLEDTEST" },
			{  "PASSWORD_ACC", "MYLEDTEST" },
			{  "IP_ACC", "192.168.5.1" },
			{  "NM_ACC", "255.255.255.0" },

			{  "DHCP" , "N" },
			{  "GW_STA" , "192.168.1.1" },
			{  "IP_STA" , "192.168.1.17" },
			{  "NM_STA" , "255.255.255.0" },
			{  "DNS_STA" , "192.168.1.1" },
			{  "SSID_STA", "" },
			{  "PASSWORD_STA" , "thP9f4s5YPd4" },
			{  "FTP_SSID", "LEDTEST" },
			{  "FTP_PW", "LEDTEST" },
			{  "loglevel" , "6" },  // 0:no log ... 6:verbose


			/*********************************
			 * IMAGES                        *
			 * *******************************/

			// images may be defined by user
			{ "fin_image_left_pos_fact", "0.375"},
			{ "fin_image_background_size", "30% 40%" }, // X{%] Y[%]  regex:Syntax/^\d{2}%\s+\d{2}%$/

			{  "topImage", "/ForestWork.jpg" },
			// {  "topConfigImage", "/schluessel.gif" },  // configuration image
			{  "ftpImage", "/ftp.gif" },
			{  "equipment", "/LED.png" },	

			{ "led_0", "/rt.png"},
			{ "led_1", "/gr.png"},

			{  "log_dest", "USB" }, // USB / TELNET

		};
}

#endif
