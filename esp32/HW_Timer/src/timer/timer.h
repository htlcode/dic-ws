/**
 * @file timer.h
 * @author Thomas Greimel (thomas.greimel04@htl-salzburg.ac.at)
 * @brief HW Timer Class
 * @version 0.1
 * @date 2022-10-03
 * 
 * @copyright Copyright (c) 2022
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <Arduino.h>
#include <ArduinoLog.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/semphr.h>

#include <global/global.h>

/**
 * @class HW_Timer "timer/timer.h"
 * @brief this class configures and runs a HW Timer
 * using a semaphore from FreeRTOS 
 */

class HW_Timer
{

public:
    enum HW_TIMER_INDEX
    {
        TIMER2 = 2,
        TIMER3
    };

private :

    static const uint8_t clk_frequ = 80; // 80MHZ
    static const bool RISING_EDGE = true;
    static const bool FALLING_EDGE = false;
    static const bool AUTORELOAD = true;
    static const bool COUNTER_UP = true;
    static const bool COUNTER_DOWN = false;

    static hw_timer_t *_timer; // lib, HAL
    static HW_TIMER_INDEX _hwTimerIndex;
    static uint16_t _prescaler; // Timer works with fosz / 3 = 240MHz / 3 = 80MHz
    static uint64_t _overflow;
    static uint8_t _counter; // 8 bit number

public:
    static SemaphoreHandle_t xTimerOverflow;

public:
    /**
     * initialize
     * @param hwTimerIndex Index of the timer to be initialized
     * @param prescaler used prescaler
     * @param overflow initialized to 1s
     * @return
     */

    bool initialize(HW_TIMER_INDEX hwTimerIndex, uint16_t prescaler = 80, uint64_t overflow = 1000000);

    /**
     * @brief starts the timer
     *
     */
    static void startTimer();

    /**
     * @brief stops the timer
     *
     */
    static void stopTimer();

    /**
     * @brief change the prescaler
     *
     * @param prescaler used prescaler
     */
    static void changePrescaler(uint16_t prescaler);

    /**
     * @brief change the overflow value
     *
     * @param overflow used overflow
     */
    static void changeOverflow(uint64_t overflow);

    /**
     * @remark this is the child process
     *
     * @param parameter is 0 by default
     */
    static void processTimer(void *parameter = 0);

    /**
     * @remark IRAM_ATTR forces code into IRAM instead of flash
     * ISR must be in internal RAM memory. Otherwise an ISR might not be loaded when
     * an interrupt occours.
     */
    static void IRAM_ATTR onTimer();
};

#endif // TIMER_H_