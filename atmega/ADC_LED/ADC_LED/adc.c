/*
 * adc.c
 *
 * Created: 23.09.2022 09:09:58
 *  Author: thomg
 */ 

#include <avr/io.h>


void initADC(void) {
	// Aufl�sung
	ADMUX |= (1<<ADLAR); // 8 bit
	
	// Referenz
	ADMUX &= ~(1<<REFS1);
	ADMUX |= (1<<REFS0);

	// Kanal
	ADMUX &= ~((1<<MUX4) | (1<<MUX3) | (1<<MUX1) | (1<<MUX0));
	ADMUX |= (1<<MUX2);
	
	// Prescaler 16
	ADCSRA |= (1<<ADPS2);
	ADCSRA &= ~((1<<ADPS1) | (1<<ADPS0));
	
	// enable
	ADCSRA |= (1<<ADEN);
	
	// Starten
	ADCSRA |= (1<<ADSC);
	return;
}

char adcDone(void) {
	if (!(ADCSRA & (1<<ADSC)))
		return 1;
	
	return 0;
}