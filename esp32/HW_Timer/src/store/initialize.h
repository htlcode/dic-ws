/*
 * store.h
 *
 *  Created on: 20.02.2017
 *      Author: reinhard
 */

#ifndef STORE_H_
#define STORE_H_
#include <stdint.h>
#include <regex>
#include <Arduino.h>
#include <FS.h>
#include <SPIFFS.h>
#include <String>
#include <map>
#include <set>
#include <ArduinoLog.h>

//#include "../helper/strhelper.h"
#include "./initDef.h"

using namespace std;

#define SPIFFS_AVAILABLE

class initialize
{

    private:

	initialize() = delete;
	~initialize() = delete;
    

    public:


	static const String getNodeValue( const String& name );
	
	
};

#endif /* STORE_H_ */
