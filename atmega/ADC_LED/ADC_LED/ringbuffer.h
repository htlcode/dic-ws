/*
 * ringbuffer.h
 *
 * Created: 20.12.2021 09:06:21
 *  Author: thomg
 */

#ifndef RINGBUFFER_H_
#define RINGBUFFER_H_

void initRingbuffer();
char print(char *);
char sendSerialData(unsigned char *, int len);

#endif /* RINGBUFFER_H_ */