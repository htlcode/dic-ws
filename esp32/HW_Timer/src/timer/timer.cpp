/**
 * @file timer.cpp
 * @author Thomas Greimel (thomas.greimel04@htl-salzburg.ac.at)
 * @brief
 * @version 0.1
 * @date 2022-10-03
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "timer.h"

uint8_t HW_Timer::_counter = 0;
HW_Timer::HW_TIMER_INDEX HW_Timer::_hwTimerIndex = HW_Timer::HW_TIMER_INDEX::TIMER2;

hw_timer_t *HW_Timer::_timer = NULL;

uint16_t HW_Timer::_prescaler = 0;
uint64_t HW_Timer::_overflow = 0;

SemaphoreHandle_t HW_Timer::xTimerOverflow = xSemaphoreCreateBinary();

bool HW_Timer::initialize(HW_Timer::HW_TIMER_INDEX hwTimerIndex, uint16_t prescaler = 80, uint64_t overflow = 1000000)
{
    _hwTimerIndex = hwTimerIndex;
    _overflow = overflow;
    _prescaler = prescaler;

    HW_Timer::xTimerOverflow = xSemaphoreCreateBinary();

    uint8_t taskCore = 1; // core 1
    // build child process
    xTaskCreatePinnedToCore(
                &processTimer, // task function
                "processTimer", // task name
                4000,           // stack size
                NULL,           // no args
                3,              // priority
                NULL,           // store task handle
                taskCore        // core to execute
    );

    return true;
}

void HW_Timer::processTimer(void *parameter)
{

    Log.verbose("<%s> start timer child process\n", __FUNCTION__);

    for (;;)
    {
        /**
         * @brief task move to Block state to wait for interrupt event
         * xTimerOverflow semaphore to take
         * portMAX_DELAY the maximum delay to wait for semaphore
         */

        xSemaphoreTake(HW_Timer::xTimerOverflow, portMAX_DELAY);
        _counter++;
        _counter = _counter % 256;

        Serial.printf("%s %x\n", __FUNCTION__, _counter);

        if (_counter % 2)
        {
            digitalWrite(led0, 1);
        }
        else
        {
            digitalWrite(led0, 0);
        }
    }
}

void IRAM_ATTR HW_Timer::onTimer() {
    BaseType_t xHigherPiorityTaskWoken = pdFALSE;

    xSemaphoreGiveFromISR(HW_Timer::xTimerOverflow, &xHigherPiorityTaskWoken);
}