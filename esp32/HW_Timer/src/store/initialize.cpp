/*
 * store.cpp
 *
 *  Created on: 20.02.2017
 *      Author: reinhard
 */

#include "initialize.h"



/***************************
 *  public members         *
 ***************************/


const String initialize::getNodeValue(const String& name)
{
    std::map<String,String>::const_iterator it = init_def::_initDefault.find(name);
    if ( it == init_def::_initDefault.end())  // item in _init available ?
    {
		String value = "";
		return value;

    }
    return (*it).second;
}


