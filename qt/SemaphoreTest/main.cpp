#include <QCoreApplication>
#include <QThread>
#include <QSemaphore>
#include <QDebug>
#include <QList>
#include <QQueue>

#define BUFFERSIZE 20
#define DATALEN 100

int buffer[BUFFERSIZE];

QSemaphore sFreeBytes(BUFFERSIZE);
QSemaphore sSentBytes(0);

// PRODUCER
class Thread1 : public QThread {

public:
    void run() {
        int sum = 0;

        while (sum < DATALEN)
        {
            sFreeBytes.acquire(BUFFERSIZE);
            for (int i = 0; i < BUFFERSIZE && sum < DATALEN; i++)
            {
                qDebug() << "Thread1 " + QString::number(i);
            }
            sSentBytes.release(BUFFERSIZE);
        }

        return;
    }

};

// CONSUMER
class Thread2 : public QThread {

public:
    void run()
    {
        int sum = 0;

        while (sum < DATALEN)
        {
            sSentBytes.acquire(BUFFERSIZE);

            for (int i = 0; i < BUFFERSIZE && sum < DATALEN; i++) {
                qDebug() << "Thread2 " + QString::number(i);
                sum++;
            }
            sFreeBytes.release(BUFFERSIZE);
        }

        return;
    }
};



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Thread1 thread1;
    Thread2 thread2;

    thread1.start();
    thread2.start();

    thread1.wait();
    thread2.wait();
    qDebug() << "Threads finished";
    return 0;
}
