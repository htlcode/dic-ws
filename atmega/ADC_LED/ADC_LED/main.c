/*
 * ADC_LED.c
 *
 * Created: 23.09.2022 09:09:23
 * Author : thomg
 */

#ifndef F_CPU
#define F_CPU 16000000
#endif

#include "adc.h"
#include "ringbuffer.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

void update(char value)
{
    int voltage = ((int)value * 5) / 255;

    // set led
    if (voltage >= 5)
        PORTD &= ~(1 << PD7);
    else
        PORTD |= (1 << PD7);

    char *buffer = "xV Analog\n";
    char num = '0' + voltage;
    buffer[0] = num;
    print(buffer);
    return;
}

int main(void)
{
    DDRD |= (1 << PD7);
    PORTD |= (1 << PD7);

    initADC();
    initRingbuffer();
    sei();

    while (1)
    {
        if (adcDone())
        {
            char val = ADCH;
            update(val);
            ADCSRA |= (1 << ADSC);
            _delay_ms(100);
        }
    }
}