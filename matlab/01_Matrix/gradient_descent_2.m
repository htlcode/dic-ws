X = [2104 1416 1534 852;    % (4x4)
     5 3 3 2; 
     1 2 2 1; 
     45 40 30 36; 
     ];

Theta = [1;3;7;4;-2];        % (5x1)

Y = [460 232 315 178];      % (1x5)

% normieren
Z = (X - mean(X,2))./std(X,0,2);
Xnorm = [ones(1, 4); Z];

% Kosten berechnen
E = ((Theta' * Xnorm) - Y);
Cost = 1/(2*size(X,2)) * sum(E.^2);
Cost_neu = 1;

% änderung der Kosten soll minimal werden
while abs(Cost - Cost_neu) > 1e-3
    Cost = Cost_neu;

    % Kosten berechnen
    E = ((Theta' * Xnorm) - Y);
    Cost_neu = 1/(2*size(X,2)) * sum(E.^2);
    
    % dJ/dTheta
    E = ((Theta' * Xnorm) - Y) * Xnorm';
    dJdTheta = 1 / size(Y,2) * E;
   
    % neues Theta berechen
    Theta = Theta - 0.001 * dJdTheta';
end