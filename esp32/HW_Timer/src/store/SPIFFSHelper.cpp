/*
 * SPIFFSHelper.cpp
 *
 *  Created on: 21.05.2018
 *      Author: reinhard
 */

#include "SPIFFSHelper.h"

std::set<String> SPIFFSHelper::_fileList = {};
std::set<String> SPIFFSHelper::_fileFilterList = {};

using namespace std;

void SPIFFSHelper::listDir(fs::FS &fs, const char * dirname, uint8_t levels)
{
    Serial.printf("Listing directory: %s\r\n", dirname);

    if ( SPIFFS.begin() == false )
    {
    	Serial.println("- failed to mount SPIFFS");
    	_fileList.empty();
    	return;
    }

    File root = fs.open(dirname);
    if(!root)
    {
        Serial.println("- failed to open directory");
        _fileList.empty();
        return;
    }
    if(!root.isDirectory())
    {
        Serial.println(" - not a directory");
        return;
    }

    File file = root.openNextFile();
    while(file)
    {
        if(file.isDirectory())
        {
            Serial.print("  DIR : ");
            Serial.println(file.name());
            if(levels)
            {
                listDir(fs, file.name(), levels -1);
            }
        }
        else
        {
            Serial.printf("FILE: <%s> SIZE:<%d>\n", file.name(),  file.size() );
            _fileList.insert(file.name());
        }
        file = root.openNextFile();
    }
}

const std::set<String>& SPIFFSHelper::getExtFilterfileList(const String& ext)
{
	String fileExt = "";
	for (std::set<String>::const_iterator it = _fileList.begin(); it != _fileList.end(); ++it)
	{
		int index = (*it).indexOf(".");
		fileExt = (*it).substring(index+1);
		fileExt.toLowerCase();

		if ( fileExt == ext )
		{
			_fileFilterList.insert( (*it) );
		}
	}
	return _fileFilterList;
}

const std::set<String>& SPIFFSHelper::getExtFilterfileList( const std::set<String>& extList  )
{
	const char * path = "/";
	SPIFFSHelper::listDir(SPIFFS, path, 0);

	for (std::set<String>::const_iterator it = extList.begin(); it != extList.end(); ++it)
	{
		getExtFilterfileList( *it );
	}
	return _fileFilterList;
}

const std::set<String>& SPIFFSHelper::getFileList()
{
	return _fileList;
}

String SPIFFSHelper::serialize()
{
	String List = "";
	 for (std::set<String>::const_iterator it = _fileList.begin(); it != _fileList.end(); ++it)
	 {
		 List += *it + " ";
	 }
	 if ( List.length() > 1 )
		 List = List.substring(0, List.length()-1); // remove last blank

	 return List;
}

bool SPIFFSHelper::format()
{
		Serial.printf("RESET SPIFF please wait\n");

		if ( !SPIFFS.begin() )
		{
			if ( SPIFFS.begin(FORMATONFAIL) == false )
				return false;

			SPIFFS.end();
		}

		bool succ = SPIFFS.format();
		SPIFFS.end();
		SPIFFS.begin();
		Serial.printf("RESET SPIFF done\n");
		return succ;
}


