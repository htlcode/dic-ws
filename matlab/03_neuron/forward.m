X= [1;
    30;
    15;
    2];

theta1 = [1 2 3 4;
         2 4 6 8;
         -3 -1 -2 -4];

theta2 = [-1 2 -2 1];

%  X    a2  a3
%   th1  th2
%  O - 
%  O -- O
%  O -- O - O
%  O -- O

z = theta1 * X;
a2 = [1;sigmoid(z)];

z = theta2 * a2;
a3 = sigmoid(z);

