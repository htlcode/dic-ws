#include <Arduino.h>
#include "global/global.h"

void setup()
{
    Serial.begin(9600);
    Serial.printf("%s Hello ESP32\n", __FUNCTION__);

    pinMode(led0, OUTPUT);
    digitalWrite(led0, 1);

    return;
}

void loop()
{
    Serial.printf("%s LED ein\n", __FUNCTION__);
    digitalWrite(led0, HIGH);
    delay(1000);
    Serial.printf("%s LED aus\n", __FUNCTION__);
    digitalWrite(led0, LOW);
    delay(1000);
    return;
}