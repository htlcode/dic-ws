X = [1 1 1 1;
     2 -4 3 8;
     4 0.1 -3 -1];

Y = [10 30 20 4];

Theta = [1;
         4;
        -3];

E = (Theta' * X) - Y;
dJdTheta0 = 1 / length(Y) * E * X(1, :)';
dJdTheta1 = 1 / length(Y) * E * X(2, :)';

Theta0_new = Theta(1) - 0.1 * dJdTheta0;
Theta1_new = Theta(2)- 0.1 * dJdTheta1;