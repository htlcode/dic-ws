/*
 * ringbuffer.c
 *
 * Created: 20.12.2021 09:06:04
 *  Author: thomg
 */

#include <avr/io.h>
#include <avr/interrupt.h>

#define RINGBUFFERSIZE 20

unsigned char ringbuffer[RINGBUFFERSIZE];
unsigned char *pRead, *pWrite;

void initUSART0()
{
	UBRR0 = 51;
	UCSR0C |= ((1 << UCSZ01) | (1 << UCSZ00));
	UCSR0B |= ((1 << TXEN0) | (1 << RXEN0));
}

ISR(USART0_UDRE_vect)
{

	if (pRead == pWrite)
	{
		UCSR0B &= ~(1 << UDRIE0);
		return;
	}

	UDR0 = *pRead;
	pRead++;
	if (pRead >= &ringbuffer[RINGBUFFERSIZE])
	{
		pRead = ringbuffer;
	}
}

void initRingbuffer()
{
	initUSART0();
	// pRead = ringbuffer[0]; -> zeigt auf die n�chsten zu sendenden Daten
	// pWrite = ringbuffer[0]; -> zeit auf die n�chste freie Stelle
	pRead = pWrite = ringbuffer;
}

// pWrite > pRead: freeSize = RINGBUFFERSIZE - pWrite + pRead - 1
// pRead > pWrite: freeSize = pRead - pWrite
// die freeSize wird um 1 dekrementiert, damit der pWrite Pointer nicht auf die gleiche Stelle trifft wie der pRead.
// wir wollen den Buffer leer definieren, wenn pRead == pWrite

char sendSerialData(unsigned char *myData, int len)
{
	int freeSize;

	// enter critical section
	unsigned char cSreg;
	cSreg = SREG;
	cli();

	if (pWrite >= pRead)
	{
		freeSize = RINGBUFFERSIZE - (int)pWrite + (int)pRead - 1;
	}
	else
	{
		freeSize = pRead - pWrite - 1;
	}

	// leave critical section
	SREG = cSreg;

	if (len > freeSize)
		return -1; // no memory

	for (int i = 0; i < len; i++)
	{
		*pWrite = myData[i];
		pWrite++;
		if (pWrite >= &ringbuffer[RINGBUFFERSIZE])
		{
			pWrite = ringbuffer;
		}
	}

	UCSR0B |= (1 << UDRIE0);

	return 0;
}

char print(char *str)
{
	char *p = str;
	int i = 0;
	while (*p != '\0')
	{
		i++;
		p++;
	}
	return sendSerialData((unsigned char *)str, i);
}