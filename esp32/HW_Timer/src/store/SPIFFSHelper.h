/*
 * SPIFFSHelper.h
 *
 *  Created on: 21.05.2018
 *      Author: reinhard
 */

#ifndef STORE_SPIFFSHELPER_H_
#define STORE_SPIFFSHELPER_H_

#include <Fs.h>
#include <SPIFFS.h>
#include <String>
#include <set>
#include <string>
#include <ArduinoLog.h>

using namespace std;

#define DEBUG_SPIFFS_HELPER

#define FORMATONFAIL false

class SPIFFSHelper
{

private:
	static std::set<String> _fileList;
	static std::set<String> _fileFilterList;
	

public:
	SPIFFSHelper() = delete;
	virtual ~SPIFFSHelper() = delete;

	static bool format();
	static void listDir(fs::FS &fs, const char * dirname, uint8_t levels);

	static const std::set<String>& getExtFilterfileList( const std::set<String>& extList  );
	static const std::set<String>& getExtFilterfileList( const String& ext);
	
	static const std::set<String>& getFileList();
	static String serialize();

};

#endif /* STORE_SPIFFSHELPER_H_ */
