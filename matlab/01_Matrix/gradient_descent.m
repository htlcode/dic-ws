X = [1 1 1 1;
     1 2 3 4];
Y = [28 60 40 60];
Theta = [40; 3.3333333333333333333333];

% dJ/dTheta0 berechnen
E = (Theta' * X) - Y;
dJdTheta0 = 1 / length(Y) * E * X(1, :)';


Theta_neu = Theta(1) - 0.1 * dJdTheta0;
