/*
 * adc.h
 *
 * Created: 23.09.2022 09:10:27
 *  Author: thomg
 */ 


#ifndef ADC_H_
#define ADC_H_

void initADC(void);
char adcDone(void);

#endif /* ADC_H_ */